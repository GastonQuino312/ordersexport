const express = require("express");
const bodyParser = require("body-parser");
const XlsxPopulate = require("xlsx-populate");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const app = express();
app.use(express.static(__dirname));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  );
  res.setHeader("Content-Type", "application/json");
  next();
});

var path = require("path");

app.get("/getAllReferences", function(req, res) {
  let apiUsername = "StraightcurveGa2AU";
  let apiConnectKey = "993f6936ccc344b9a5e5bced301905bf";

  var x = new XMLHttpRequest();
  x.open(
    "GET",
    //"http://0.0.0.0:8080/" +
    "https://" +
      "api.cin7.com/api/v1/" +
      "SalesOrders" +
      "?fields=reference&page=1&rows=250"
  );
  // I put "XMLHttpRequest" here, but you can use anything you want.

  x.setRequestHeader(
    "Authorization",
    "Basic " + Buffer.from(apiUsername + ":" + apiConnectKey).toString("base64")
  );
  x.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  x.onload = function() {
    let orders = JSON.parse(x.responseText);
    console.log(orders);

    res.send(JSON.stringify(orders));
  };

  x.send();
});

app.get("/ref/", function(req, res) {
  let herokuUrl = "http://127.0.0.1:8080/";
  let apiUsername = "StraightcurveGa2AU";
  let apiConnectKey = "993f6936ccc344b9a5e5bced301905bf";
  let apiBaseUrl = "api.cin7.com/api/v1";
  let rowCount = 0;
  let startRow = 8;
  let startCol = 2;
  XlsxPopulate.fromFileAsync("./Example.xlsx").then(workbook => {
    // Modify the workbook.
    var x = new XMLHttpRequest();
    var ws = workbook.sheet("Picklist");
    x.open(
      "GET",
      //"http://0.0.0.0:8080/" +
      "https://" +
        "api.cin7.com/api/v1/" +
        "SalesOrders" +
        "?fields=id,reference,deliveryCompany,deliveryCity,deliveryCountry,lineItems&page=1&rows=250"
    );
    // I put "XMLHttpRequest" here, but you can use anything you want.

    x.setRequestHeader(
      "Authorization",
      "Basic " +
        Buffer.from(apiUsername + ":" + apiConnectKey).toString("base64")
    );
    x.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    x.onload = function() {
      let orders = JSON.parse(x.responseText);
      console.log(orders);

      var xhrOptions = new XMLHttpRequest();
      xhrOptions.open(
        "GET",
        //"http://0.0.0.0:8080/" +
        "https://" +
          "api.cin7.com/api/v1/" +
          "ProductOptions" +
          "?fields=id,SupplierCode,Option1,Option2&page=1&rows=250"
      );
      // I put "XMLHttpRequest" here, but you can use anything you want.

      xhrOptions.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      xhrOptions.setRequestHeader(
        "Authorization",
        "Basic " +
          Buffer.from(apiUsername + ":" + apiConnectKey).toString("base64")
      );
      xhrOptions.onload = function() {
        let options = JSON.parse(xhrOptions.responseText);

        let myOptions = {};
        for (let k = 0; k < options.length; k++) {
          let option = options[k];
          myOptions[option.id.toString()] = { ...option };
        }

        for (let i = 0; i < orders.length; i++) {
          let order = orders[i];
          console.log(i);
          // ws.row(startRow + rowCount)
          //   .cell(startCol)
          //   .value(order.reference);
          // ws.row(startRow + rowCount)
          //   .cell(startCol + 1)
          //   .value(order.deliveryCompany);
          // ws.row(startRow + rowCount)
          //   .cell(startCol + 2)
          //   .value(order.deliveryCity);
          // ws.row(startRow + rowCount)
          //   .cell(startCol + 3)
          //   .value(order.deliveryCountry);

          //rowCount++;
          if (!order.lineItems) continue;
          for (let j = 0; j < order.lineItems.length; j++) {
            let item = order.lineItems[j];

            let itemProductOption = {
              supplierCode: "",
              option1: "",
              option2: ""
            };
            if (myOptions[item.productOptionId.toString()])
              itemProductOption = {
                ...myOptions[item.productOptionId.toString()]
              };

            ws.row(startRow + rowCount)
              .cell(startCol)
              .value(order.reference);
            ws.row(startRow + rowCount)
              .cell(startCol + 1)
              .value(order.deliveryCompany);
            ws.row(startRow + rowCount)
              .cell(startCol + 2)
              .value(order.deliveryCity);
            ws.row(startRow + rowCount)
              .cell(startCol + 3)
              .value(order.deliveryCountry);
            ws.row(startRow + rowCount)
              .cell(startCol + 4)
              .value(item.code);
            ws.row(startRow + rowCount)
              .cell(startCol + 5)
              .value(itemProductOption.supplierCode);
            ws.row(startRow + rowCount)
              .cell(startCol + 6)
              .value(item.name);
            ws.row(startRow + rowCount)
              .cell(startCol + 7)
              .value(itemProductOption.option1);
            ws.row(startRow + rowCount)
              .cell(startCol + 8)
              .value(itemProductOption.option2);
            ws.row(startRow + rowCount)
              .cell(startCol + 9)
              .value(item.qty);
            rowCount++;
          }
        }
        workbook
          .toFileAsync("./report.xlsx")
          .then(() => {
            //res.download(__dirname + "/report.xlsx");
            return res.send({ downloadUrl: "/report-salesorder-all.xlsx" });
          })
          .catch(error => {
            res.send({ error: "Error" });
          });
      };
      xhrOptions.send();
    };

    x.send();
  });
});
app.get("/ref/:ref", function(req, res) {
  let herokuUrl = "http://127.0.0.1:8080/";
  let apiUsername = "StraightcurveGa2AU";
  let apiConnectKey = "993f6936ccc344b9a5e5bced301905bf";
  let apiBaseUrl = "api.cin7.com/api/v1";
  let rowCount = 0;
  let startRow = 23;
  let startCol = 2;
  XlsxPopulate.fromFileAsync("./Example.xlsx").then(workbook => {
    // Modify the workbook.
    var x = new XMLHttpRequest();
    var ws = workbook.sheet("Picklist");
    x.open(
      "GET",
      //"http://0.0.0.0:8080/" +
      "https://" + "api.cin7.com/api/v1/" + "SalesOrders" + "?page=1&rows=250"
    );
    // I put "XMLHttpRequest" here, but you can use anything you want.

    x.setRequestHeader(
      "Authorization",
      "Basic " +
        Buffer.from(apiUsername + ":" + apiConnectKey).toString("base64")
    );
    x.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    x.onload = function() {
      let orders = JSON.parse(x.responseText);
      let order = null;
      console.log(orders);

      for (let i in orders) {
        let tempOrder = orders[i];
        if (tempOrder.reference == req.params.ref) {
          order = { ...tempOrder };
          break;
        }
      }

      if (order == null) {
        res.send({
          error: `No such sales order : reference = ${req.params.ref}`
        });
      }

      var xhrOptions = new XMLHttpRequest();
      xhrOptions.open(
        "GET",
        //"http://0.0.0.0:8080/" +
        "https://" +
          "api.cin7.com/api/v1/" +
          "ProductOptions" +
          "?fields=id,SupplierCode,Option1,Option2&page=1&rows=250"
      );
      // I put "XMLHttpRequest" here, but you can use anything you want.

      xhrOptions.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      xhrOptions.setRequestHeader(
        "Authorization",
        "Basic " +
          Buffer.from(apiUsername + ":" + apiConnectKey).toString("base64")
      );
      xhrOptions.onload = function() {
        let options = JSON.parse(xhrOptions.responseText);

        let myOptions = {};
        for (let k = 0; k < options.length; k++) {
          let option = options[k];
          myOptions[option.id.toString()] = { ...option };
        }
        ws.row(13)
          .cell(3)
          .value(order.reference);
        ws.row(14)
          .cell(3)
          .value(order.deliveryCompany);
        ws.row(15)
          .cell(3)
          .value(order.deliveryCity);
        ws.row(16)
          .cell(3)
          .value(order.deliveryCountry);
        if (order.lineItems) {
          for (let j = 0; j < order.lineItems.length; j++) {
            let item = order.lineItems[j];

            let itemProductOption = {
              supplierCode: "",
              option1: "",
              option2: ""
            };
            if (myOptions[item.productOptionId.toString()])
              itemProductOption = {
                ...myOptions[item.productOptionId.toString()]
              };

            ws.row(startRow + rowCount)
              .cell(startCol)
              .value(item.code);
            ws.row(startRow + rowCount)
              .cell(startCol + 1)
              .value(itemProductOption.supplierCode);
            ws.row(startRow + rowCount)
              .cell(startCol + 2)
              .value(item.name);
            ws.row(startRow + rowCount)
              .cell(startCol + 3)
              .value(itemProductOption.option1);
            ws.row(startRow + rowCount)
              .cell(startCol + 4)
              .value(itemProductOption.option2);
            ws.row(startRow + rowCount)
              .cell(startCol + 5)
              .value(item.qty);
            rowCount++;
          }
        }

        workbook
          .toFileAsync("./report-salesorder-" + req.params.ref + ".xlsx")
          .then(() => {
            /*res.download(
              __dirname + "/report-salesorder-" + req.params.ref + ".xlsx"
            );*/
            return res.send({
              downloadUrl: "/report-salesorder-" + req.params.ref + ".xlsx"
            });
          })
          .catch(error => {
            res.send({ error: "Error" });
          });
      };
      xhrOptions.send();
    };

    x.send();
  });
});
//______________ port listen ________________________
var server = app.listen(Number(process.env.PORT || 8000), () => {
  console.log("Listening on port %d", server.address().port);
});
